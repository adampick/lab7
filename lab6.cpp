/*
//
Adam Pickeral
CPSC 1021, Section 1, Fall 20
apicker@g.clemson.edu
TA: Nushrat

*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

#define ARRSIZE 10

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

    int i;

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
    employee employees[ARRSIZE];
    
    for(i = 0; i < ARRSIZE; i++){
	cout << endl << "What is employee # " << i+1 << "'s first name? : ";
	cin >> employees[i].firstName;
	
	cout << "What is their last name? : "; 
	cin >> employees[i].lastName;

	cout << "What year were they born? : "; 
	cin >> employees[i].birthYear;


	cout << "What is their hourly wage? : "; 
	cin >> employees[i].hourlyWage;
    }
/*


  *After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

    random_shuffle(employees, &(employees[ARRSIZE]),myrandom);


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/

    employee smaller_list[ARRSIZE/2];

    for(i = 0; i < ARRSIZE/2; i++)
	smaller_list[i] = employees[i];


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
    sort(employees,&(employees[ARRSIZE]),name_order);

    /*Now print the array below */

    for(i = 0; i < ARRSIZE/2; i++){
	cout << endl << endl << "Sorted Student # " << i+1;
	cout << endl << "Name: " << smaller_list[i].firstName << " " << smaller_list[i].lastName;
	cout << endl << "Year of Birth: " << smaller_list[i].birthYear;
	cout << endl << "Hourly Wage : " << smaller_list[i].hourlyWage;

    }


    cout << endl << endl;

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
 
    if(lhs.lastName < rhs.lastName){
	return true;
    }
    else{
	return false;
    }
}

